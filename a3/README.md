# LIS4368

## Cy Yates

### Assignment #3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include at least 10 records in each table
3. Provide Bitbucket read-only acces to cdy15 repo and inlude links to all of the following files:
	- docs folder: a3.mwb and a3.sql
	- img folder: a3.png
	- README.md
4. Links: Bitbucket Repo

#### README.md file should include the following items:

- Screenshot of A3 ERD


#### Assignment Screenshot and Links:

*Screenshot of A3 ERD*:

![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

*A3 Documents*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")


#### Links:

*Bitbucket Repo:*
<https://bitbucket.org/cdy15/lis4368>

*Web Portfolio for A3*:
<http://localhost:9999/lis4368/a3/index.jsp>

