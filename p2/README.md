# LIS4368

## Cy Yates

### Project #2 Requirements:

*Deliverables:*

1. Desing a web application that uses CRUD functionality
2. Be able to display list of customers in application

#### README.md file should include the following items:

- Screenshot of valid user form
- Screenshot of passed validation
- Screenshot of displayed data
- Screenshot of modified data
- Screenshot of delete warning
- Screenshot of associated changes

#### Project Screenshot and Links:

*Valid user form*:

![USER INPUT](img/p2_data.png "User Data")

*Screenshot of Successful Validation*:

![SUCCESSFUL VALIDATION](img/p2_success.png "Data was Successfully Validated")

*Screenshot of Displayed Data*

![DISPLAYED DATA](img/p2_display.png "Data Display")

*Screenshot of Data being Modified*

![DATA BEING MODIFIED](img/p2_modify.png "Data Being Modified")

*Screenshot of Modified Data*

![MODIFIED DATA](img/p2_modified.png "Data Modified")

*Screenshot of Deletion Warning*

![DELETE DATA](img/p2_delete.png "Data Deletion")

*Screenshot of Associated Changes*

![ASSOCIATED CHANGES](img/p2_changes.png "Data Changes")

#### Links:

*Web Portfolio for P2*:
<http://localhost:9999/lis4368/customerform.jsp?assign_num=p2>

