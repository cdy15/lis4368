> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cy Yates

### Assignment #2 Requirements:

*Three Parts:*

1. Download MySQL
2. Deploy Web Servlets 
3. Questions from Chapters 5 and 6

#### README.md file should include the following items:

* Assessment links


#### Assignment Screenshots:

*Screenshot of <http://localhost:9999/hello/index.html>*:

![http://localhost:9999/hello/index.html Screenshot](img/index.png)

*Screenshot of <http://localhost:9999/hello/sayhello>*:

![http://localhost:9999/hello/sayhello Screenshot](img/hello.png)

*Screenshot of <http://localhost:9999/hello/querybook.html>*:

![http://localhost:9999/hello/querybook.html Screenshot](img/querybook.png)

*Screenshot of <http://localhost:9999/hello/query?author=Tan+Ah+Teck>*:

![http://localhost:9999/hello/query?author=Tan+Ah+Teck Screenshot](img/queryresults.png)

*Screenshot of <http://localhost:9999/hello/sayhi>*:

![http://localhost:9999/hello/sayhi Screenshot](img/hellov2.png)



#### Tutorial Links:

*Install MySQL:*
[Install MySQL Tutorial Link]
<https://www.ntu.edu.sg/home/ehchua/programming/sql/MySQL_HowTo.html>

*Deploy a WebApp:*
[Deploy a WebApp Tutuorial]
<https://www.ntu.edu.sg/home/ehchua/programming/howto/Tomcat_HowTo.html>
