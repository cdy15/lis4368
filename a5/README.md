# LIS4368

## Cy Yates

### Assignment #5:

*Deliverables:*

1. Show Server-Side Validation
2. Provide Screenshot to inserted data
3. Provide Bitbucket read-only acces to cdy15 repo

#### README.md file should include the following items:

- Screenshot of Valid User Form Entry
- Screenshot of Passed Validation
- Screenshot of Associated Database Entry


#### Assignment Screenshot and Links:

*Screentshot of Valid User Form Entry*:
![Valid User Form Entry](img/valid.png "Valid User Form Entry")

*Screenshot of Passed Validation*:

![Passed Validation](img/passed.png "Passed Validation")

*Screenshot of Associated Database Entry*:

![Associated Database Entry](img/data.png "Associated Database Entry")

#### Links:

*Bitbucket Repo:*
<https://bitbucket.org/cdy15/lis4368>

*My Web Portfolio*:
<http://localhost:9999/lis4368/index.jsp>

