> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368

## Cy Yates

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstation locations and myteamquotes)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Download MySQL
    - Deploy Web Servlets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Have MySQL running on a local connection
    - Create an Entity Relationship Diagram (ERD)
    - Include data
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Basic Server-Side Validation
    - JSP Forms
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server-Side Validation
    - Insert Data into Local DB
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Desing a Web Application to Validate Data
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Design a Web Application that uses CRUD functionality
    - Be able to Correctly Dislpay List of Data

