> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Cy Yates

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost9999
* git commands w/short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorial above 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1.	git init � Allows you to create an empty Git repository 
2.	git status � Displays the state of the directory currently being used
3.	git add � Allows you to add file contents 
4.	git commit � Make changes to the current repository
5.	git push � Allows you to update your remote repositories with your data
6.	git pull � Allows you to grab data from another repository
7.	git checkout � Enables you to switch from one branch to another


#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost9999*:

![Screenshot of running http://localhost9999:](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
<https://bitbucket.org/cdy15/bitbucketstationlocations>

*Tutorial: Request to update a teammate's repository:*
<https://bitbucket.org/cdy15/myteamquotes>
