import java.util.Scanner;

class asciiApp
{
   public static void main(String[] args)
   {

        int val = 0;
        
        Scanner input = new Scanner(System.in);

        System.out.println("Printing Characters A-Z as ASCII values:");

        System.out.println("Character A has ASCII value 65");
        System.out.println("Character B has ASCII value 66");
        System.out.println("Character C has ASCII value 67");
        System.out.println("Character D has ASCII value 68");
        System.out.println("Character E has ASCII value 69");
        System.out.println("Character F has ASCII value 70");
        System.out.println("Character G has ASCII value 71");
        System.out.println("Character H has ASCII value 72");
        System.out.println("Character I has ASCII value 73");
        System.out.println("Character J has ASCII value 74");
        System.out.println("Character K has ASCII value 75");
        System.out.println("Character L has ASCII value 76");
        System.out.println("Character M has ASCII value 77");
        System.out.println("Character N has ASCII value 78");
        System.out.println("Character O has ASCII value 79");
        System.out.println("Character P has ASCII value 80");
        System.out.println("Character Q has ASCII value 81");
        System.out.println("Character R has ASCII value 82");
        System.out.println("Character S has ASCII value 83");
        System.out.println("Character T has ASCII value 84");
        System.out.println("Character U has ASCII value 85");
        System.out.println("Character V has ASCII value 86");
        System.out.println("Character W has ASCII value 87");
        System.out.println("Character X has ASCII value 88");
        System.out.println("Character Y has ASCII value 89");
        System.out.println("Character Z has ASCII value 90 \n");

        System.out.println("Printing ASCII values 48-122 as characters:");

        System.out.println("ASCII value 48 has character value 0");
        System.out.println("ASCII value 49 has character value 1");
        System.out.println("ASCII value 50 has character value 2");
        System.out.println("ASCII value 51 has character value 3");
        System.out.println("ASCII value 52 has character value 4");
        System.out.println("ASCII value 53 has character value 5");
        System.out.println("ASCII value 54 has character value 6");
        System.out.println("ASCII value 55 has character value 7");
        System.out.println("ASCII value 56 has character value 8");
        System.out.println("ASCII value 57 has character value 9");
        System.out.println("ASCII value 58 has character value :");
        System.out.println("ASCII value 59 has character value ;");
        System.out.println("ASCII value 60 has character value <");
        System.out.println("ASCII value 61 has character value =");
        System.out.println("ASCII value 62 has character value >");
        System.out.println("ASCII value 63 has character value ?");
        System.out.println("ASCII value 64 has character value @");
        System.out.println("ASCII value 65 has character value A");
        System.out.println("ASCII value 66 has character value B");
        System.out.println("ASCII value 67 has character value C");
        System.out.println("ASCII value 68 has character value D");
        System.out.println("ASCII value 69 has character value E");
        System.out.println("ASCII value 70 has character value F");
        System.out.println("ASCII value 71 has character value G");
        System.out.println("ASCII value 72 has character value H");
        System.out.println("ASCII value 73 has character value I");
        System.out.println("ASCII value 74 has character value J");
        System.out.println("ASCII value 75 has character value K");
        System.out.println("ASCII value 76 has character value L");
        System.out.println("ASCII value 77 has character value M");
        System.out.println("ASCII value 78 has character value N");
        System.out.println("ASCII value 79 has character value O");
        System.out.println("ASCII value 80 has character value P");
        System.out.println("ASCII value 81 has character value Q");
        System.out.println("ASCII value 82 has character value R");
        System.out.println("ASCII value 83 has character value S");
        System.out.println("ASCII value 84 has character value T");
        System.out.println("ASCII value 85 has character value U");
        System.out.println("ASCII value 86 has character value V");
        System.out.println("ASCII value 87 has character value W");
        System.out.println("ASCII value 88 has character value X");
        System.out.println("ASCII value 89 has character value Y");
        System.out.println("ASCII value 90 has character value Z");
        System.out.println("ASCII value 91 has character value [");
        System.out.println("ASCII value 92 has character value '\'");
        System.out.println("ASCII value 93 has character value ]");
        System.out.println("ASCII value 94 has character value ^");
        System.out.println("ASCII value 95 has character value -");
        System.out.println("ASCII value 96 has character value `");
        System.out.println("ASCII value 97 has character value a");
        System.out.println("ASCII value 98 has character value b");
        System.out.println("ASCII value 99 has character value c");
        System.out.println("ASCII value 100 has character value d");
        System.out.println("ASCII value 101 has character value e");
        System.out.println("ASCII value 102 has character value f");
        System.out.println("ASCII value 103 has character value g");
        System.out.println("ASCII value 104 has character value h");
        System.out.println("ASCII value 105 has character value i");
        System.out.println("ASCII value 106 has character value j");
        System.out.println("ASCII value 107 has character value k");
        System.out.println("ASCII value 108 has character value l");
        System.out.println("ASCII value 109 has character value m");
        System.out.println("ASCII value 110 has character value n");
        System.out.println("ASCII value 111 has character value o");
        System.out.println("ASCII value 112 has character value p");
        System.out.println("ASCII value 113 has character value q");
        System.out.println("ASCII value 114 has character value r");
        System.out.println("ASCII value 115 has character value s");
        System.out.println("ASCII value 116 has character value t");
        System.out.println("ASCII value 117 has character value u");
        System.out.println("ASCII value 118 has character value v");
        System.out.println("ASCII value 119 has character value w");
        System.out.println("ASCII value 120 has character value x");
        System.out.println("ASCII value 121 has character value y");
        System.out.println("ASCII value 122 has character value z \n");

        System.out.println("Allowing user ASCII value input:");

        if( val != -1 ) {

            System.out.print("Please enter ASCII value (32 - 127):");
            val = input.nextInt();

            if( val < 32 || val > 127 ){ 
                System.out.println("ASCII value must be >= 32 and <= 127\n");
            }

        if( val == 32 ) {
           System.out.println("ASCII value 32 has character value [space]\n");
        }

        else if( val == 33 ) {
        System.out.println("ASCII value 33 has character value !\n");
        }

        if( val == 34 ) {
        System.out.println("ASCII value 34 has character value ''\n");
        }

        else if( val == 35 ) {
            System.out.println("ASCII value 35 has character value #\n");
        }

        if( val == 36 ) {
            System.out.println("ASCII value 36 has character value $\n");
        }
    
        else if( val == 37 ) {
            System.out.println("ASCII value 37 has character value %\n");
        }

        if( val == 38 ) {
            System.out.println("ASCII value 38 has character value &\n");
        }
        
        else if( val == 39 ) {
            System.out.println("ASCII value 39 has character value '\n");
        }

        if( val == 40 ) {
            System.out.println("ASCII value 40 has character value (\n");
        }
            
        else if( val == 41 ) {
            System.out.println("ASCII value 41 has character value )\n");
        }

        if( val == 42 ) {
            System.out.println("ASCII value 42 has character value *\n");
        }
    
        else if( val == 43 ) {
            System.out.println("ASCII value 43 has character value +\n");
        }

        if( val == 44 ) {
            System.out.println("ASCII value 44 has character value ,\n");
        }
    
        else if( val == 45 ) {
            System.out.println("ASCII value 45 has character value -\n");
        }

        if( val == 46 ) {
            System.out.println("ASCII value 46 has character value .\n");
        }
    
        else if( val == 47 ) {
            System.out.println("ASCII value 47 has character value /\n");
        }

        if( val == 48 ) {
            System.out.println("ASCII value 48 has character value 0\n");
        }
    
        else if( val == 49 ) {
            System.out.println("ASCII value 49 has character value 1\n");
        }

        if( val == 50 ) {
            System.out.println("ASCII value 50 has character value 2\n");
        }
            
        else if( val == 51 ) {
            System.out.println("ASCII value 51 has character value 3\n");
        }

        if( val == 52 ) {
            System.out.println("ASCII value 52 has character value 4\n");
        }
    
        else if( val == 53 ) {
            System.out.println("ASCII value 53 has character value 5\n");
        }

        if( val == 54 ) {
            System.out.println("ASCII value 54 has character value 6\n");
        }
    
        else if( val == 55 ) {
            System.out.println("ASCII value 55 has character value 7\n");
        }

        if( val == 56 ) {
            System.out.println("ASCII value 56 has character value 8\n");
        }
    
        else if( val == 57 ) {
            System.out.println("ASCII value 57 has character value 9\n");
        }

        if( val == 58 ) {
            System.out.println("ASCII value 58 has character value :\n");
        }
    
        else if( val == 59 ) {
            System.out.println("ASCII value 59 has character value ;\n");
        }

        if( val == 60 ) {
            System.out.println("ASCII value 60 has character value <\n");
         }
 
         else if( val == 61 ) {
         System.out.println("ASCII value 61 has character value =\n");
         }
 
         if( val == 62 ) {
         System.out.println("ASCII value 62 has character value >\n");
         }
 
         else if( val == 63 ) {
             System.out.println("ASCII value 63 has character value ?\n");
         }
 
         if( val == 64 ) {
             System.out.println("ASCII value 64 has character value @\n");
         }
     
         else if( val == 65 ) {
             System.out.println("ASCII value 65 has character value A\n");
         }
 
         if( val == 66 ) {
             System.out.println("ASCII value 66 has character value B\n");
         }
         
         else if( val == 67 ) {
             System.out.println("ASCII value 67 has character value C\n");
         }
 
         if( val == 68 ) {
             System.out.println("ASCII value 68 has character value D\n");
         }
             
         else if( val == 69 ) {
             System.out.println("ASCII value 69 has character value E\n");
         }
 
         if( val == 70 ) {
             System.out.println("ASCII value 70 has character value F\n");
         }
     
         else if( val == 71 ) {
             System.out.println("ASCII value 71 has character value G\n");
         }
 
         if( val == 72 ) {
             System.out.println("ASCII value 72 has character value H\n");
         }
     
         else if( val == 73 ) {
             System.out.println("ASCII value 73 has character value I\n");
         }
 
         if( val == 74 ) {
             System.out.println("ASCII value 74 has character value J\n");
         }
     
         else if( val == 75 ) {
             System.out.println("ASCII value 75 has character value K\n");
         }
 
         if( val == 76 ) {
             System.out.println("ASCII value 76 has character value L\n");
         }
     
         else if( val == 77 ) {
             System.out.println("ASCII value 77 has character value M\n");
         }
 
         if( val == 78 ) {
             System.out.println("ASCII value 78 has character value N\n");
         }
             
         else if( val == 79 ) {
             System.out.println("ASCII value 79 has character value O\n");
         }
 
         if( val == 80 ) {
             System.out.println("ASCII value 80 has character value P\n");
         }
     
         else if( val == 81 ) {
             System.out.println("ASCII value 81 has character value Q\n");
         }
 
         if( val == 82 ) {
             System.out.println("ASCII value 82 has character value R\n");
         }
     
         else if( val == 83 ) {
             System.out.println("ASCII value 83 has character value S\n");
         }
 
         if( val == 84 ) {
             System.out.println("ASCII value 84 has character value T\n");
         }
     
         else if( val == 85 ) {
             System.out.println("ASCII value 85 has character value U\n");
         }
 
         if( val == 86 ) {
             System.out.println("ASCII value 86 has character value V\n");
         }
     
         else if( val == 87 ) {
             System.out.println("ASCII value 87 has character value W\n");
         }

         else if( val == 88 ) {
            System.out.println("ASCII value 88 has character value X\n");
        }

        if( val == 89 ) {
            System.out.println("ASCII value 89 has character value Y\n");
        }
    
        else if( val == 90 ) {
            System.out.println("ASCII value 90 has character value Z\n");
        }

        if( val == 91 ) {
            System.out.println("ASCII value 91 has character value [\n");
        }
    
        else if( val == 92 ) {
            System.out.println("ASCII value 92 has character value '\'\n");
        }

        if( val == 93 ) {
            System.out.println("ASCII value 93 has character value ]\n");
        }
            
        else if( val == 94 ) {
            System.out.println("ASCII value 94 has character value ^\n");
        }

        if( val == 95 ) {
            System.out.println("ASCII value 95 has character value -\n");
        }
    
        else if( val == 96 ) {
            System.out.println("ASCII value 96 has character value `\n");
        }

        if( val == 97 ) {
            System.out.println("ASCII value 97 has character value a\n");
        }
    
        else if( val == 98 ) {
            System.out.println("ASCII value 98 has character value b\n");
        }

        if( val == 99 ) {
            System.out.println("ASCII value 99 has character value c\n");
        }
    
        else if( val == 100 ) {
            System.out.println("ASCII value 100 has character value d\n");
        }

        if( val == 101 ) {
            System.out.println("ASCII value 101 has character value e\n");
        }
    
        else if( val == 102 ) {
            System.out.println("ASCII value 102 has character value f\n");
        }

        if( val == 103 ) {
            System.out.println("ASCII value 103 has character value g\n");
         }
 
         else if( val == 104 ) {
         System.out.println("ASCII value 104 has character value h\n");
         }
 
         if( val == 105 ) {
         System.out.println("ASCII value 105 has character value i\n");
         }
 
         else if( val == 106 ) {
             System.out.println("ASCII value 106 has character value j\n");
         }
 
         if( val == 107 ) {
             System.out.println("ASCII value 107 has character value k\n");
         }
     
         else if( val == 108 ) {
             System.out.println("ASCII value 108 has character value l\n");
         }
 
         if( val == 109 ) {
             System.out.println("ASCII value 109 has character value m\n");
         }
         
         else if( val == 110 ) {
             System.out.println("ASCII value 110 has character value n\n");
         }
 
         if( val == 111 ) {
             System.out.println("ASCII value 111 has character value o\n");
         }
             
         else if( val == 112 ) {
             System.out.println("ASCII value 112 has character value p\n");
         }
 
         if( val == 113 ) {
             System.out.println("ASCII value 113 has character value q\n");
         }
     
         else if( val == 114 ) {
             System.out.println("ASCII value 114 has character value r\n");
         }
 
         if( val == 115 ) {
             System.out.println("ASCII value 115 has character value s\n");
         }
     
         else if( val == 116 ) {
             System.out.println("ASCII value 116 has character value t\n");
         }
 
         if( val == 117 ) {
             System.out.println("ASCII value 117 has character value u\n");
         }
     
         else if( val == 118 ) {
             System.out.println("ASCII value 118 has character value v\n");
         }
 
         if( val == 119 ) {
             System.out.println("ASCII value 119 has character value w\n");
         }
     
         else if( val == 120 ) {
             System.out.println("ASCII value 120 has character value x\n");
         }
 
         if( val == 121 ) {
             System.out.println("ASCII value 121 has character value y\n");
         }
             
         else if( val == 122 ) {
             System.out.println("ASCII value 122 has character value z\n");
         }
 
         if( val == 123 ) {
             System.out.println("ASCII value 123 has character value {\n");
         }
     
         else if( val == 124 ) {
             System.out.println("ASCII value 124 has character value |\n");
         }
 
         if( val == 125 ) {
             System.out.println("ASCII value 125 has character value }\n");
         }
     
         else if( val == 126 ) {
             System.out.println("ASCII value 126 has character value ~\n");
         }
 
         if( val == 127 ) {
             System.out.println("ASCII value 127 has character value [delete]\n");
         }

        } else {
            System.out.println("Invalid integer--ASCII values must be a number");
        }
    }
}