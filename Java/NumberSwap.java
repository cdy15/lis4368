import java.util.Scanner;

public class NumberSwap
{
    public static void main (String[] args)
    {
        //variables
        int num1=0;
        int num2=0;
        int temp=num1;
        boolean num1Valid = false;
        boolean num2Valid = false;
        Scanner sc = new Scanner (System.in);

        System.out.println("Note: Program checks for integers and non-numeric values.\n");

        while(num1Valid == false)
        {
            System.out.print("Please enter first number: ");
            if (sc.hasNextInt())
            {
            num1 = sc.nextInt();
            num1Valid = true;
        } else
        {
            System.out.println("Not a valid integer!\n");
            System.out.print("Please try again. ");
        }
        sc.nextLine();

        if(num1Valid == true)
        {
            System.out.println(" ");
        }

            while(num2Valid == false && num1Valid == true)
            {

                System.out.print("Please enter second number: ");
                if (sc.hasNextInt())
                {
                num2 = sc.nextInt();
                num2Valid = true;
            } else
            {
                System.out.println("Not a valid integer!\n");
                System.out.print("Please try again. ");
            }
            sc.nextLine();
        
        if(num2Valid == true) 
        {

            System.out.println("\nBefore Swapping");
            System.out.println("num1 = " + num1);
            System.out.println("num2 = " + num2);
            System.out.println(" ");
            
            temp=num1;
            num1=num2;
            num2=temp;
            

            System.out.println("After Swapping");
            System.out.println("num1 = " + num1);
            System.out.println("num2 = " + temp);
        }
    }
}
}
}