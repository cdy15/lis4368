import java.util.Scanner;

public class LargestThreeNumbers
{
    public static void main (String[] args)
    {
        //variables
        int num1=0;
        int num2=0;
        int num3=0;
        boolean num1Valid = false;
        boolean num2Valid = false;
        boolean num3Valid = false;
        Scanner sc = new Scanner (System.in);

        System.out.println("Note: Program checks for integers and non-numeric values.\n");

        while(num1Valid == false)
        {
            System.out.print("Please enter first number: ");
            if (sc.hasNextInt())
            {
            num1 = sc.nextInt();
            num1Valid = true;
        } else
        {
            System.out.println("Not a valid integer!\n");
            System.out.print("Please try again. ");
        }
        sc.nextLine();

        if(num1Valid == true)
        {
            System.out.println(" ");
        }

            while(num2Valid == false && num1Valid == true)
            {

                System.out.print("Please enter second number: ");
                if (sc.hasNextInt())
                {
                num2 = sc.nextInt();
                num2Valid = true;
            } else
            {
                System.out.println("Not a valid integer!\n");
                System.out.print("Please try again. ");
            }
            sc.nextLine();

            if(num2Valid == true)
            {
                System.out.println(" ");
            }

            while(num3Valid == false && num2Valid == true)
            {

                System.out.print("Please enter third number: ");
                if (sc.hasNextInt())
                {
                num3 = sc.nextInt();
                num3Valid = true;
            } else
            {
                System.out.println("Not a valid integer!\n");
                System.out.print("Please try again. ");
            }
            sc.nextLine();
        
        if(num3Valid == true) 
        {
            System.out.println(" ");
            if ( num1 > num2 && num1 > num3 ){
            System.out.println("First number is largest.");
            } else if ( num2 > num1 && num2 > num3 ) {
            System.out.println("Second number is largest.");
            } else if ( num3 > num1 && num3 > num2 ) {
            System.out.println("Third number is largest.");
            }
        }
    }
}
}
}
}