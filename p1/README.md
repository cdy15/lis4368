# LIS4368

## Cy Yates

### Project #1 Requirements:

*Deliverables:*

1. Desing a web application that validates data with regular expressions

#### README.md file should include the following items:

- Screenshot of P1 Failed Validation
- Screenshot of P1 Successful Validation


#### Project Screenshot and Links:

*Screenshot of Failed Validation*:

![FAILED VALIDATION](img/p1_failed.png "Data Failed to be Validated")

*Screenshot of Successful Validation*:

![SUCCESSFUL VALIDATION](img/p1_success.png "Data was Successfully Validated")


#### Links:

*Web Portfolio for P1*:
<http://localhost:9999/lis4368/p1/index.jsp>

